const mix = require('laravel-mix');
const imagemin = require('laravel-mix-imagemin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
const resourcePath = 'resources';
const publicPath = 'public';

mix.js(resourcePath + '/frontend/js/vendor.js', publicPath + '/frontend/js')
    .postCss(resourcePath + '/frontend/css/app.css', publicPath + '/frontend/css')
    .postCss(resourcePath + '/frontend/css/styles.css', publicPath + '/frontend/css')
    .sass(resourcePath + '/frontend/sass/app.scss', publicPath + '/frontend/css/fontawesome.css')
    .browserSync('http://localhost:8086')
    // minify images
    .imagemin(
        'img',
        {
            patterns: [
                {
                    from: resourcePath + '/frontend/images', to: 'frontend/images'
                }
            ]
        }
    );